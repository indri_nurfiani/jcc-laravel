<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendataan(){
        return view('halaman.pendataan');
    }

    public function welcome(Request $request){
        // dd($request->all());
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];

        return view('halaman.welcome', compact('firstName', 'lastName'));

    }
}
