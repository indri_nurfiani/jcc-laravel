@extends('layout.master')
@section('judul')
    Halaman Pendataan
@endsection
@section('content')
    <h2>Halaman Pendataan</h2>
    <form action="/kirim" method="post">
        @csrf
        <label>First Name</label> <br>
        <input type="text" name="firstName"> <br><br>
        <label>Last Name</label> <br>
        <input type="text" name="lastName"> <br><br>
        <label>Gender</label><br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br><br>
        <label>Nationality</label><br>
        <select name="country">
            <option value="indonesia">Indonesia</option>
            <option value="china">China</option>
            <option value="jerman">Jerman</option>
        </select><br><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Arabic<br>
        <input type="checkbox" name="language">Japanese<br><br>
        <label>Bio</label> <br>
        <textarea name="alamat" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection